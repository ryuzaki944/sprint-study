const tab_contents = ['images/icons/tab_content1.png', 'images/icons/tab_content2.png', 'images/icons/tab_content3.png']

function openContent(index) {
    $('.button_tabs div').removeClass('active')
    document.querySelectorAll('.button_tabs div')[index - 1].classList.add('active')
    $('.tab_content img').attr('src', tab_contents[index - 1])
}