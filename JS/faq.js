function toggleAnswer(answer) {
    if(answer.style.maxHeight) {
        answer.style.maxHeight = null
        answer.style.padding = 0
    }
    else {
        answer.style.maxHeight = answer.scrollHeight + 'px'
        answer.style.padding = 5 + '%'
    }
}