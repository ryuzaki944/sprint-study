$(document).ready(function() {
	let btn = $("#input_btn");
	let xName = $("#input_name");
	let phone = $("#input_phone");
	let err = $(".error_field");
	let err_input = $(".err_input")
	let modal_win = $("#modal_win_thq");
	let span_close = $(".modal_win_close");

	btn.click(function(){
		if(xName.val().length <= 0){
			err_input.css("border-bottom","1px solid #f51119");
			err.html('*Заполните поле');
		}
		else if(phone.val().length <= 0){
			err_input.css("border-bottom","1px solid #f51119");
			err.text('*Заполните поле');
		}
		else{
			console.log(xName.val());
			console.log(phone.val());
			err_input.css("border-bottom","1px solid #000").val("");
			err.text("\u00A0");
			modal_win.css("display","block");
		}		
	});

	span_close.click(function(){
		modal_win.css("display","none");
	});

	modal_win.click(function(){
		modal_win.css("display","none");
	});

})

	// xName.keyup(function(){
	// 	err.text('\u00A0');
	// });

